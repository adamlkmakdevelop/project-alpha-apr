from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import LoginView, SignUp


# Create your views here.
def user_login(request):
    if request.method == "POST":
        form = LoginView(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

        user = authenticate(
            request,
            username=username,
            password=password,
        )

        if user is not None:
            login(request, user)
            return redirect("list_projects")
    else:
        form = LoginView()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def sign_up(request):
    if request.method == "POST":
        form = SignUp(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

        if password == password_confirmation:
            user = User.objects.create_user(
                username,
                password=password,
            )
            login(request, user)
            return redirect("list_projects")
        else:
            form.add_error("password", "the passwords do not match")
    else:
        form = SignUp()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)
